from django.db import models
from django.contrib.auth.models import User
from api.fields import UUIDField

import datetime
# Create your models here.

class Books(models.Model):
  title = models.CharField(max_length=255, blank=False)
  published_date = models.DateField(blank=True)
  created = models.DateTimeField(auto_now_add=True, editable=False)

  def __unicode__(self):
    return (self.title)

  class Meta:
    verbose_name_plural = "Books"

class Session(models.Model):
    session_id = UUIDField(primary_key=True, editable=False)
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    last_active = models.DateTimeField(default=datetime.datetime.now)