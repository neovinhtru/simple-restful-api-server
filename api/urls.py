from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('api.views',
     url(r'^login/', 'api_login', name='login'),
     url(r'(?P<obj>[a-z]*)/(?P<pk>[0-9]+)/$', 'books_api_object_detail',name='books_api_object_detail'),
     url(r'(?P<obj>[a-z]*)/$', 'books_api_object_list', name='books_api_object_list'),
)