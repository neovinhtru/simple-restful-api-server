import datetime
from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth.models import AnonymousUser

try:
    from threading import local
except ImportError:
    from django.utils._threading_local import local

_thread_locals = local()


def get_current_user():
    user = getattr(_thread_locals, 'user', None)
    if not user:
        raise ImproperlyConfigured("No current user found")
    return user


class CurrentUserMiddleware(object):

    def process_request(self, request):
        if hasattr(request, 'user') and request.user and request.user.is_authenticated():
            _thread_locals.user = request.user
        else:
            _thread_locals.user = None


class APIAuthenticationMiddleware(object):

    def process_request(self, request):
        if not hasattr(request, 'user'):
            request.user = AnonymousUser()
        session_id = request.META.get('HTTP_AUTHORIZATION')
        request.session_id = session_id
        if session_id:
            try:
                from api.models import Session
                session_id = session_id.replace("Adhoc ", "")
                session = Session.objects.select_related('account').get(session_id=session_id)
                request.user = session.user
                setattr(_thread_locals, 'user', request.user)
                session.last_active = datetime.datetime.now()
                session.save()
            except Session.DoesNotExist:
                request.user = AnonymousUser()