import sys
import json
from rest_framework.renderers import JSONRenderer
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import (HttpResponse, HttpResponseForbidden, HttpResponseNotAllowed)
from api.forms import APIUserAuthenticationForm
from middleware import get_current_user
from models import Books
from api.serializers import BooksSerializer

class JSONResponse(HttpResponse):
    """
    HttpResponse that renders it's content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@csrf_exempt
def api_login(request):
    if request.method == "POST":
        form = APIUserAuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            User.objects.get(pk=form.user_cache.id)
            ret = HttpResponse(json.dumps({
                'session_id': form.api_session_id_cache,
                'user_id': form.user_cache.id,
            }), content_type="application/json")
        else:
            ret = HttpResponseForbidden(json.dumps(form.errors), content_type="application/json")
    else:
        ret = HttpResponseNotAllowed(['POST', ])
    return ret

@csrf_exempt
def books_api_object_list(request, obj):

  ser_str = "%sSerializer" % obj.capitalize()

  if request.method == 'GET':
    query = Books.objects.all()
    serializer = (getattr(sys.modules[__name__], ser_str))(query, many=True)
    return JSONResponse(serializer.data)

  if request.method == 'POST':
    try:
      get_current_user()
    except Exception as ex:
      return HttpResponse(ex.message, status=404)
    data = json.loads(request.body)
    serializer = (getattr(sys.modules[__name__], ser_str))(data=data)
    if serializer.is_valid():
      serializer.save()
      return JSONResponse(serializer.data, status=201)
    else:
        return JSONResponse(serializer.errors, status=400)

@csrf_exempt
def books_api_object_detail(request, obj, pk):
  ser_str = "%sSerializer" % obj.capitalize()

  if request.method == "GET":
    query = Books.objects.filter(pk=pk)
    serializer = (getattr(sys.modules[__name__], ser_str))(query, many=True)
    if len(serializer.data) !=0:
      return JSONResponse(serializer.data)
    else:
      return JSONResponse(serializer.data, status=404)

  if request.method == "DELETE":
    try:
      get_current_user()
    except Exception as ex:
      return HttpResponse(ex.message, status=404)
    item = Books.objects.filter(pk=pk)
    try:
        item.delete()
        return HttpResponse(status=200)
    except Exception as ex:
        return HttpResponse(status=404)


