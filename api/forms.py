from django import forms
from django.contrib.auth import authenticate
from lockout import LockedOut
from models import Session
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _

ERROR_MESSAGE = _("Please enter a correct email and password. ")
ERROR_MESSAGE_INACTIVE = _("This account is inactive.")
ERROR_LOGOUT = _("Your account has been locked due to too many incorrect login attempts. Try logging in again after 15 minutes.")

class APIUserAuthenticationForm(AuthenticationForm):
    message_incorrect_password = ERROR_MESSAGE
    message_inactive = ERROR_MESSAGE_INACTIVE
    message_logout = ERROR_LOGOUT

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        try:
            if username and password:
                self.user_cache = authenticate(username=username, password=password)
                if self.user_cache is None:
                    raise forms.ValidationError(self.message_incorrect_password)
                if not self.user_cache.is_active:
                    raise forms.ValidationError(self.message_inactive)
                try:
                    session = Session.objects.create(user = self.user_cache)
                    self.api_session_id_cache = session.session_id
                except AttributeError:
                    pass
        except LockedOut:
            raise forms.ValidationError(self.message_logout)
        return self.cleaned_data

