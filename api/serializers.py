from rest_framework import serializers
from api.models import Books

class ObjectRelatedField(serializers.RelatedField):
    def to_native(self, value):
        """
        Assign nested serializer to correct object.
        """
        if isinstance(value, Books):
            serializer = BooksSerializer(value)
        else:
            raise Exception('Unexpected type of tagged object')

        return serializer.data

class BooksSerializer(serializers.ModelSerializer):

    class Meta:
        model = Books
        fields = ('id', 'title', 'published_date')