-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 17, 2014 at 04:32 PM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restful_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_books`
--

CREATE TABLE IF NOT EXISTS `api_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `published_date` date NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `api_books`
--

INSERT INTO `api_books` (`id`, `title`, `published_date`, `created`) VALUES
(3, 'A test book3', '2014-03-17', '2014-03-17 10:00:08'),
(4, 'A test book3', '2014-03-17', '2014-03-17 10:49:27'),
(5, 'A test book3', '2014-03-17', '2014-03-17 10:51:07'),
(6, 'A test book3', '2014-03-17', '2014-03-17 10:52:30'),
(7, 'A test book 12', '2014-03-17', '2014-03-17 16:08:13'),
(8, 'A test book 12', '2014-03-17', '2014-03-17 16:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `api_session`
--

CREATE TABLE IF NOT EXISTS `api_session` (
  `session_id` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `last_active` datetime NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `api_session_fbfc09f1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `api_session`
--

INSERT INTO `api_session` (`session_id`, `user_id`, `created`, `last_active`) VALUES
('0538a0d1-176e-42cb-8ce1-fb3342bffc20', 1, '2014-03-17 05:24:07', '2014-03-17 05:24:07'),
('0c443a6a-be07-4c12-bada-ab4318ead448', 1, '2014-03-17 06:27:55', '2014-03-17 06:27:55'),
('2cbbd958-a039-444f-8b0c-23dfccf9dbe8', 1, '2014-03-17 13:45:40', '2014-03-17 13:45:40'),
('48234c77-60f4-43cb-9d9b-a79d286c4fff', 1, '2014-03-17 07:32:03', '2014-03-17 07:32:03'),
('5bdc8e51-f6e6-42b7-a31e-3d3e68a68994', 1, '2014-03-17 16:05:10', '2014-03-17 16:05:10'),
('66b1ae79-8269-4017-853f-0d97cc013eea', 1, '2014-03-17 16:21:54', '2014-03-17 16:21:54'),
('6a5e6621-bb49-4d12-aad1-3ecdb50c4886', 1, '2014-03-17 06:18:33', '2014-03-17 06:18:33'),
('6beb66e0-3fe1-4417-a97d-02383d319f72', 1, '2014-03-17 07:51:13', '2014-03-17 07:51:13'),
('73f3c197-e658-4d88-9158-e46d1dc4c432', 1, '2014-03-17 07:14:43', '2014-03-17 07:14:43'),
('75aa3171-61f7-44bc-a764-75e1dfefeca6', 1, '2014-03-17 10:20:53', '2014-03-17 10:52:30'),
('77145e1e-4ff9-487b-ae7a-2e5156f4c382', 1, '2014-03-17 07:29:28', '2014-03-17 07:29:28'),
('777d03ab-1890-4077-9f7f-dac49b4276ea', 1, '2014-03-17 07:58:55', '2014-03-17 09:57:56'),
('92da2642-9076-47b0-8f71-5429f8983f8c', 1, '2014-03-17 09:59:00', '2014-03-17 10:32:24'),
('92fb8475-2f5a-420a-884b-92c99a7236f0', 1, '2014-03-17 06:17:55', '2014-03-17 06:17:55'),
('a08964ce-60c3-482b-a4d0-830c1f486a4c', 1, '2014-03-17 05:24:24', '2014-03-17 05:24:24'),
('a26e882a-c8b9-4f5b-9c9f-6b0abefbb666', 1, '2014-03-17 06:09:00', '2014-03-17 06:09:00'),
('b39eac75-1dba-4297-8ced-f699f6e06ca2', 1, '2014-03-17 16:30:34', '2014-03-17 16:30:34'),
('b3dcacdf-a1ec-411d-b911-3637457912a1', 1, '2014-03-17 06:27:46', '2014-03-17 06:27:46'),
('b4d63cc9-95ff-4184-9adc-cc0f621b545a', 1, '2014-03-17 06:44:10', '2014-03-17 06:44:10'),
('ba70855c-9bfc-48c1-9dd0-e3202e3122cf', 1, '2014-03-17 07:56:10', '2014-03-17 07:56:51'),
('be50a0f4-1de4-4f1f-8e97-74e66c72f44e', 1, '2014-03-17 16:22:14', '2014-03-17 16:22:14'),
('c4dd3111-3176-4f61-b35d-d88195652a3b', 1, '2014-03-17 16:07:03', '2014-03-17 16:10:05'),
('c7f0f753-8af1-4ae9-8508-818e43ee848d', 1, '2014-03-17 16:21:37', '2014-03-17 16:21:37'),
('d50e33d5-d646-4d29-bee0-f3b8061bd58a', 1, '2014-03-17 16:13:20', '2014-03-17 16:13:20'),
('d8d1a6d4-5114-4ca0-bd68-0ad90616643a', 1, '2014-03-17 07:48:09', '2014-03-17 07:48:09'),
('db2ef4f4-7474-4a0f-ac07-c0caafe87c44', 1, '2014-03-17 06:29:13', '2014-03-17 06:29:13'),
('db859beb-aee3-4f2f-801b-d661356c72e7', 1, '2014-03-17 16:22:25', '2014-03-17 16:22:25'),
('e259288f-f07b-4b8d-9b3d-d2003643e47b', 1, '2014-03-17 06:07:29', '2014-03-17 06:07:29'),
('e5a0b9c9-8b8f-45b2-8493-5a35bc4a15e8', 1, '2014-03-17 06:29:17', '2014-03-17 06:29:17');

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add permission', 1, 'add_permission'),
(2, 'Can change permission', 1, 'change_permission'),
(3, 'Can delete permission', 1, 'delete_permission'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add user', 3, 'add_user'),
(8, 'Can change user', 3, 'change_user'),
(9, 'Can delete user', 3, 'delete_user'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add session', 5, 'add_session'),
(14, 'Can change session', 5, 'change_session'),
(15, 'Can delete session', 5, 'delete_session'),
(16, 'Can add site', 6, 'add_site'),
(17, 'Can change site', 6, 'change_site'),
(18, 'Can delete site', 6, 'delete_site'),
(19, 'Can add log entry', 7, 'add_logentry'),
(20, 'Can change log entry', 7, 'change_logentry'),
(21, 'Can delete log entry', 7, 'delete_logentry'),
(22, 'Can add migration history', 8, 'add_migrationhistory'),
(23, 'Can change migration history', 8, 'change_migrationhistory'),
(24, 'Can delete migration history', 8, 'delete_migrationhistory'),
(25, 'Can add books', 9, 'add_books'),
(26, 'Can change books', 9, 'change_books'),
(27, 'Can delete books', 9, 'delete_books'),
(28, 'Can add session', 10, 'add_session'),
(29, 'Can change session', 10, 'change_session'),
(30, 'Can delete session', 10, 'delete_session');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `is_staff`, `is_active`, `is_superuser`, `last_login`, `date_joined`) VALUES
(1, 'admin', '', '', 'vutuananh5@gmail.com', 'pbkdf2_sha256$10000$gZCgCXVoXlbr$hdkbhkIqM57RKTI0fXUsuuUNHT2gj9HEBos3HYYkU+0=', 1, 1, 1, '2014-03-17 14:04:56', '2014-03-17 13:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'permission', 'auth', 'permission'),
(2, 'group', 'auth', 'group'),
(3, 'user', 'auth', 'user'),
(4, 'content type', 'contenttypes', 'contenttype'),
(5, 'session', 'sessions', 'session'),
(6, 'site', 'sites', 'site'),
(7, 'log entry', 'admin', 'logentry'),
(8, 'migration history', 'south', 'migrationhistory'),
(9, 'books', 'api', 'books'),
(10, 'session', 'api', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('09ad6bdde4980ed3c613a3ac363dcbcb', 'MWM2MDA2NWIxZDhjNGFiNWU1YzMwNWYzZjI1NDkxNTkwZjkxMWZmYzqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n', '2014-03-31 14:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE IF NOT EXISTS `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `south_migrationhistory`
--

CREATE TABLE IF NOT EXISTS `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `south_migrationhistory`
--

INSERT INTO `south_migrationhistory` (`id`, `app_name`, `migration`, `applied`) VALUES
(1, 'api', '0001_initial', '2014-03-17 13:43:38'),
(2, 'api', '0002_auto__add_session', '2014-03-17 13:43:39');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `api_session`
--
ALTER TABLE `api_session`
  ADD CONSTRAINT `user_id_refs_id_b3451afa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
